import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { SharedComponent } from './shared/shared.component';
import { OfflineComponent } from './shared/offline/offline.component';
import { DialogComponent } from './shared';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { AppRoutingModule } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SelectivePreloadingStrategyService } from './shared/services/selective-preloading-strategy.service';
import { AppCommonModule } from './app-common.module';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { Angulartics2Module } from 'angulartics2';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { PagesModule } from './pages/pages.module';
import { SideNavComponent } from './layout/sidenav/sidenav.component';
// import { UpdateService } from './shared/services/update-service.service';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LayoutComponent,
    SideNavComponent,
    FooterComponent,
    DialogComponent,
    SharedComponent,
    OfflineComponent,
  ],
  imports: [
    BrowserModule,
		BrowserAnimationsModule,
		AppCommonModule,
    HttpClientModule,
    PagesModule,
    AppRoutingModule,
		Angulartics2Module.forRoot({
			pageTracking: {
				clearIds: true,
				idsRegExp: new RegExp('^[a-z]\\d+$'), /* Workaround: No NgModule metadata found for 'AppModule' */ // example changes /project/a01/feature to /project/feature
				clearQueryParams: true,
				clearHash: true,
			}
		}),
  ],
  providers: [
		SelectivePreloadingStrategyService,
		CookieService,
		// UpdateService,
		Title,
		{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
