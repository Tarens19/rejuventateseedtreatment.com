import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';

@Injectable()
export class UpdateService {

	constructor(public updates: SwUpdate) {
		if (updates.isEnabled) {
			interval(6 * 60 * 60).subscribe(() => updates.checkForUpdate()
				.then(() => console.log('checking for updates')));
		}
		// Check service worker if there is an updated version of The Mortgage Partner.
		//   if (swUpdate.isEnabled) {
		// 	  interval(6 * 60 * 60).subscribe(() => swUpdate.checkForUpdate()
		// 		  .then(() => console.log('checking for updates')));

		// 	  // this.swUpdate.available.subscribe(() => {

		// 	  // 	if (confirm("New version available. Load new version of The Mortgage Partner?")) {

		// 	  // 		window.location.reload();
		// 	  // 	}
		// 	  // });
		//   }
	}

	public checkForUpdates(): void {
		console.log('Checking if an update is available.');
		this.updates.available.subscribe(event => this.promptUser());
	}

	// private promptUser(): void {
	//   console.log('updating to new version');
	//   this.updates.activateUpdate().then(() => document.location.reload())); 
	// }
	private promptUser(): void {
		console.log('New version available.');
		this.updates.available.subscribe(() => {

			if (confirm("New version available. Load new version of The Mortgage Partner?")) {

				window.location.reload();
			}
		});
	}
}