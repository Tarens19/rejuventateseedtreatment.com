
/*
   Shared Module is used to import:
    1) Do declare components, directives, and pipes when those items will be re-used and referenced by the components declared in other feature modules.
    2) Avoid providing services in shared modules.
    3) Do export all symbols from the SharedModule that other feature modules need to use.
    4) Avoid specifying app-wide singleton providers in a SharedModule. Intentional singletons are OK. Take care.
*/
export { OfflineComponent } from './offline/offline.component';
export { DialogComponent } from './dialog/dialog.component';
