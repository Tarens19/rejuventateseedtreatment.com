import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import { AppCommonModule } from '../app-common.module';

@NgModule({
    imports: [
        AppCommonModule,
        TextMaskModule,
    ],
    declarations: [

        // CloudinaryUploadComponent
    ],
    providers: [
        // Services here
    ],
    exports: [
        AppCommonModule,
        TextMaskModule,
        // CloudinaryUploadComponent
    ],
    entryComponents: [
        // Dialogs, bottomsheets. etc. here.

    ]
})
export class SharedModule { }
