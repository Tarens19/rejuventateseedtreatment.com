import { Component, OnInit } from '@angular/core';
import { AppState } from 'src/app/models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
		public appState: AppState
  ) { }

  ngOnInit(): void {
  }

  closeDrawer() {
    this.appState.default.showNavigationDrawer = !this.appState.default.showNavigationDrawer
  }

}
