
import { Inject, OnDestroy } from '@angular/core';
import { AfterContentInit, Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Angulartics2 } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { AppState } from './models/app-state-model';
// import { UpdateService } from './shared/services/update-service.service';
import { filter, map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { fxArray } from './models/prefix';
declare let ga: Function;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	animations: fxArray
})


export class AppComponent implements OnInit, AfterContentInit, OnDestroy {
	title = 'rejuvenate';
	showDrawer: boolean = false;
	isLoadingApp: boolean = true;
	showNavigationDrawer: boolean;

	constructor(
		@Inject(DOCUMENT) private doc: any,
		// private sw: UpdateService,
		private router: Router,
		private route: ActivatedRoute,
		private angulartics2: Angulartics2,
		private angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
		private titleService: Title,
		public appState: AppState
	) {
		this.showNavigationDrawer = appState.default.showNavigationDrawer;
	}

	ngOnInit(): void {
		//Called after the constructor, initializing input properties, and the first call to ngOnChanges.
		//Add 'implements OnInit' to the class.

		this.router.events.pipe(untilDestroyed(this)).subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) return;
			document.body.scrollTop = 0;
		});
		// check the service worker for updates
		// this.sw.checkForUpdates();

		if (typeof ga !== 'undefined') {
			this.router.events.subscribe(event => {
				if (event instanceof NavigationEnd) {
					// angulartics2GoogleAnalytics.pageTrack(event.urlAfterRedirects);
					ga('set', 'page', 'Rejuvenate' + event.urlAfterRedirects);
					ga('send', 'pageview');
					// if (this.trackerAdded) ga('businessTracker.send', 'pageview');
				}
			});
		}
		this.angulartics2GoogleAnalytics.startTracking();

		const appTitle = this.titleService.getTitle();
		this.router
			.events.pipe(
				filter(event => event instanceof NavigationEnd),
				map(() => {
					let child = this.route.firstChild;
					while (child.firstChild) {
						child = child.firstChild;
					}
					if (child.snapshot.data['title']) {
						return child.snapshot.data['title'];
					}
					return appTitle;
				})
			).subscribe((ttl: string) => {
				if (this.appState.default.showNavigationDrawer === true) this.appState.default.showNavigationDrawer = !this.appState.default.showNavigationDrawer;
				this.appState.default.appTitle = ttl;
				// Set the title to the amount of the new notifications the user has.
				if (this.appState.default.titleCount != 0) this.titleService.setTitle('(' + this.appState.default.titleCount + ') ' + 'Mortgage Partner - ' + ttl);
				// Else return the normal title.
				else this.titleService.setTitle('Rejuvenate Seed Treatment - ' + ttl);
			});
	}

	ngAfterContentInit(): void {
		//Called after ngOnInit when the component's or directive's content has been initialized.
		//Add 'implements AfterContentInit' to the class.
		this.isLoadingApp = false;
	}

	ngOnDestroy(): void { }


	closeDrawer() {
		this.appState.default.showNavigationDrawer = !this.appState.default.showNavigationDrawer
	}

}
