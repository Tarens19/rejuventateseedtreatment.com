import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import { AppCommonModule } from '../app-common.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages.routing';

@NgModule({
	imports: [
		AppCommonModule,
		TextMaskModule,
		PagesRoutingModule,
	],
	declarations: [
		PagesComponent,
		AboutUsComponent,
		ContactUsComponent,
		HomepageComponent

	],
	providers: [
		// Services here
	],
	exports: [],
	entryComponents: [
		// Dialogs, bottomsheets. etc. here.
	]
})
export class PagesModule {
	constructor() { console.log('@ModuleCreated::PagesModule'); }
}
