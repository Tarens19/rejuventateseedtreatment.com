import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfflineComponent } from '../shared/offline/offline.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PagesComponent } from './pages.component';


const routes: Routes = [
	{
		path: '',
		component: PagesComponent,
		children: [
			{ path: '404', component: OfflineComponent, data: { reason: '404', title: '404' } },
			{ path: '', component: HomepageComponent, data: { title: 'Home' } },
			{ path: 'home', component: HomepageComponent, data: { title: 'Home' } },
			{ path: 'about', component: AboutUsComponent, data: { title: 'About' } },
			{ path: 'contact', component: ContactUsComponent, data: { title: 'Contact' } },
			{ path: '**', redirectTo: '/404', pathMatch: 'full' }
		]
		// { path: 'blog', loadChildren: './pages/blog/blog.module#BlogModule' },
		// { path: 'events', loadChildren: '../calendar/calendar.module#TAPCalendarModule', data: {title: 'Calendar'} },
		// { path: 'account', loadChildren: '../account-settings/account/account.module#AccountModule', data: {title: 'Account settings'} },
		// { path: 'settings', loadChildren: './settings/manager-settings.module#ManagerSettingsModule' },
		// { path: 'team', loadChildren: './teams/team.module#TeamModule', data: {title: 'Groups'} },
		// { path: 'booking', loadChildren: './booking/booking.module#BookingModule' },
		// { path: 'staff', loadChildren: './staff/staff.module#StaffModule', data: {title: 'Staff'} },
		// { path: 'announcements', loadChildren: './messages/messages.module#MessagesModule', data: {title: 'Announcements'} },
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule { }
